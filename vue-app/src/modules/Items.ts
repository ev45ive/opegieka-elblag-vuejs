import { Module } from "../../node_modules/vuex";
import { Item } from "@/model/item.interface";
import axios from "axios";

interface ItemsState {
  items: { [id: number]: Item };
  list: number[];
  selectedId: number;
}

export const Items: Module<ItemsState, {}> = {
  state: {
    items: {
      123: {
        id: 123,
        title: "Stored Item",
        completed: true,
        color: "#00ff00",
        created: "01-01-01 10:30:00"
      },
      234: {
        id: 234,
        title: "Stored Placki",
        completed: false,
        color: "#ff0000",
        created: "01-01-01 15:30:00"
      },
      345: {
        id: 345,
        title: "Stored Pierogi",
        completed: false,
        color: "#0000ff",
        created: "01-01-01 07:30:00"
      }
    },
    list: [123, 234, 345],
    selectedId: 123
  },
  mutations: {
    load(state, items: Item[]) {
      state.list = [];
      items.forEach(item => {
        state.items[item.id] = item;
        state.list.push(item.id);
      });
      state.selectedId = state.list[0];
    },
    insert(state, item: Item) {
      state.items[item.id] = item;
      state.list.push(item.id);
    },
    remove(state, item: Item) {
      state.list = state.list.filter(i => i !== item.id);
    },
    update(state, draft: Item) {
      state.items[draft.id] = draft;
    },
    select(state, id) {
      state.selectedId = id;
    }
  },
  actions: {
    select({ commit }, id) {
      commit("select", id);
    },
    save({ commit }, draft: Item) {
      commit("update", draft);
    },
    addItem({ commit, dispatch, getters }, title) {
      const time = new Date();
      const item: Item = {
        id: Date.now(),
        title,
        completed: false,
        color: "#ffffff",
        created: time.toString()
      };
      commit("insert", item);
      return item;
    },
    removeItem({ commit }, item: Item) {
      commit("remove", item);
    },
    fetchTodos({ commit }) {
      return axios.get("http://localhost:3000/todos?_limit=20").then(resp => {
        commit("load", resp.data);
      });
    },
    saveTodo({ commit, dispatch }, item) {
      return axios
        .put("http://localhost:3000/todos/" + item.id, item)
        .then(() => {
          return dispatch("fetchTodos");
        })
        .then(() => {
          commit("select", item.id);
        });
    }
  },
  getters: {
    items(state) {
      return state.list.map(id => state.items[id]);
    },
    selected(state) {
      return state.items[state.selectedId];
    }
  }
};

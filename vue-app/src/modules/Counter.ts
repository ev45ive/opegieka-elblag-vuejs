import { Module } from '../../node_modules/vuex';

interface CounterState {
  counter: number;
}

export const Counter: Module<CounterState, {}> = {
  namespaced:true,
  state: {
    counter: 0
  },
  // commit(<name>, param)
  mutations: {
    increment(state, param) {
      state.counter++;
    },
    decrement(state) {
      state.counter--;
    }
  },
  // dispatch(<name>, param)
  actions: {},
  getters: {
    counter(state) {
      return state.counter;
    }
  }
};
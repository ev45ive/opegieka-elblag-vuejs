export interface Item {
  id: number;
  title: string;
  completed: boolean;
  color: string;
  created: string;
}
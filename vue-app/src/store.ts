import Vue from "vue";
import Vuex, { Module } from "vuex";
import { Item } from "./model/item.interface";
import { Counter } from '@/modules/Counter';
import { Items } from './modules/Items';

Vue.use(Vuex);


export default new Vuex.Store({
  modules: {
    counter: Counter,
    items: Items
  }
});
